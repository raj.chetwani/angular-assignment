// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBdH7XNO8jsAJS7tOWAgF_SEvyoOXTCQRw",
    authDomain: "angular-79d79.firebaseapp.com",
    databaseURL: "https://angular-79d79.firebaseio.com",
    projectId: "angular-79d79",
    storageBucket: "angular-79d79.appspot.com",
    messagingSenderId: "12377051802",
    appId: "1:12377051802:web:a3730b81c9c9edf8920523",
    measurementId: "G-101GGJ90B1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
