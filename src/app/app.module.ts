import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from '../environments/environment';
import { GeneratorComponent } from './generator/generator.component';
import { GeneralComponent } from './general/general.component';
import { MyMemesComponent } from './my-memes/my-memes.component';
import { BookmarkedComponent } from './bookmarked/bookmarked.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderNavComponent,
    DashboardComponent,
    GeneratorComponent,
    GeneralComponent,
    MyMemesComponent,
    BookmarkedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig), // imports firebase/firestore
    AngularFirestoreModule,
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
  ],
  providers: [AngularFireModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
