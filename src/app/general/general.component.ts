import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {

  constructor(public afs: AngularFirestore,public authService:AuthService) { }

  memes = []

  ngOnInit(){

  this.getMemes()

  }

  async getMemes() {
    const snapchot = await this.afs.collection('memes').ref.get();
    return new Promise(resolve => {
        const v = snapchot.docs.map(x => {
            this.memes.push(x.data())
        });
        resolve(v);
    });
}

  
}
