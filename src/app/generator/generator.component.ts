import { Component, OnInit, ViewChild } from '@angular/core';
import {Meme} from '../meme.model'
import {Router} from '@angular/router'
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.css']
})


export class GeneratorComponent implements OnInit {

  @ViewChild('memeCanvas', { static: false }) myCanvas;

  topText: String = '';
  bottomText: String = '';
  fileEvent: any;
  textColor: String = '#000000';
  backgroundColor: String = '#F9F9FB';

  newMeme: Meme = {
    name:'',
    memeID:'',
    imageURL: '',
    likes: 0,
  };

  user = {}

  constructor(public afs: AngularFirestore,public router: Router,public authService: AuthService) { }
  

  handleChange = (e) => {
    this[e.target.name] = e.target.value
  }


  ngOnInit(): void {
  }

  preview(e: any) {
    this.fileEvent = e;
    
    let canvas = this.myCanvas.nativeElement;
    let ctx = canvas.getContext('2d');

    let render = new FileReader();
    render.readAsDataURL(e.target.files[0]);
    render.onload = function (event) {
      const img = new Image();

      img.src = event.target.result as string;

      img.onload = function () {
        ctx.drawImage(img, 50, 150, 600, 500);
      } 
    } 
  }

  drawText() {
    let canvas = this.myCanvas.nativeElement;
    let ctx = canvas.getContext('2d');

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.fillStyle = this.backgroundColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    this.preview(this.fileEvent);

    ctx.fillStyle = this.textColor;
    ctx.font = '50px Comic Sans MS';
    ctx.textAlign = 'center';
    ctx.fillText(this.topText, canvas.width/2, 100);
    ctx.fillText(this.bottomText, canvas.width/2, 750);
  }

  upload = async () => {
    let canvas = this.myCanvas.nativeElement;
    let image = canvas.toDataURL('image/png');
    
    this.newMeme.id = (JSON.parse(localStorage.getItem('user'))).user.uid
    this.newMeme.name = (JSON.parse(localStorage.getItem('user'))).user.displayName
    this.newMeme.imageURL = image;
    this.newMeme.memeID = (Math.random()*1000).toString()
    this.afs.collection('memes').doc(this.newMeme.memeID).set(this.newMeme)
    this.router.navigate(['dashboard/general'])

  }


}