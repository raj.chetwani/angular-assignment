import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GeneratorComponent } from './generator/generator.component';
import { GeneralComponent } from './general/general.component';
import { MyMemesComponent } from './my-memes/my-memes.component';
import { BookmarkedComponent } from './bookmarked/bookmarked.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'dashboard/general',
    pathMatch:'full'
  },
  {
    path:'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: 'generator', // child route path
        component: GeneratorComponent, // child route component that the router renders
      },
      {
        path: 'general', // child route path
        component: GeneralComponent, // child route component that the router renders
      },
      {
        path: 'mymemes', // child route path
        component: MyMemesComponent, // child route component that the router renders
      },
      {
        path: 'bookmarked', // child route path
        component: BookmarkedComponent, // child route component that the router renders
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
