export interface Meme {
    id?: string;
    memeID?:string
    name?:string;
    imageURL?: string;
    likes: 0;
}