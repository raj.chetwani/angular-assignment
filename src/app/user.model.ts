export interface User {
    id?: string;
    name?:string;
    bookmarked:string[];
    liked?:string[];
}