import { Injectable } from '@angular/core';
import  { auth} from 'firebase/app'
import { AngularFireAuth } from "@angular/fire/auth";
import {Router} from '@angular/router'
import {User} from './user.model'
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    public afs: AngularFirestore
  ) { }

  authenticated = false
  photoURL = ''
  userID = ''
  
  user: User = {
    id:'',
    name:'',
    bookmarked:[],
    liked:[]
  }

  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }  


  async AuthLogin(provider) {

    let user = await this.afAuth.auth.signInWithPopup(provider)
    localStorage.setItem('user', JSON.stringify(user))

    this.userID = user.user.uid
    this.photoURL = user.user.photoURL
    this.authenticated = true
    this.router.navigate(['dashboard/generator'])

    const dataUser = this.afs.collection('users').doc(this.userID).ref
    
    dataUser.get().then((docSnapshot) => {
          if(!docSnapshot.exists){
            
            this.user = {

              id:this.userID,
              name: user.user.displayName,
              bookmarked:[],
              liked:[]
      
            }
            this.afs.collection('users').doc(this.userID).set(this.user)
          }

          this.setBookmark(docSnapshot.data().bookmarked)
          this.setLiked(docSnapshot.data().liked)
    })
    }


  logout = () => {
    this.afAuth.auth.signOut()
    localStorage.setItem('user', "")
    this.router.navigate(['/'])
    this.authenticated = false
    this.userID = ""
  }


  setBookmark = (book) => {
    this.user.bookmarked = book
  }

  setLiked = (liked) => {
    this.user.liked = liked
  }


  addBookmark = (id) => {

    this.user.bookmarked.push(id)
  
    const dataUser = this.afs.collection('users').doc(this.userID).ref;
  
    dataUser.set({
      bookmarked : this.user.bookmarked
    },{merge:true})
  
    
    }
  
    removeBookmark = (id) => {
      this.user.bookmarked = this.user.bookmarked.filter((item) => item!== id)
    
      const dataUser = this.afs.collection('users').doc(this.userID).ref;
    
      dataUser.set({
        bookmarked : this.user.bookmarked
      },{merge:true})
    
    }
  


  addLike = (id) => {

    this.user.liked.push(id)

    const dataUser = this.afs.collection('users').doc(this.userID).ref;
  
    dataUser.set({
      liked : this.user.liked
    },{merge:true})
  

    const memeData = this.afs.collection('memes').doc(id).ref;


    memeData.get().then((docSnapshot) => {
         memeData.set({
            likes: docSnapshot.data().likes + 1
          },{merge:true})      
    })

  }

  removeLike = (id) => {
    this.user.liked = this.user.liked.filter((item) => item!== id)
  
    const dataUser = this.afs.collection('users').doc(this.userID).ref;
  
    dataUser.set({
      liked : this.user.liked
    },{merge:true})

    const memeData = this.afs.collection('memes').doc(id).ref;
    memeData.get().then((docSnapshot) => {
      memeData.set({
        likes: docSnapshot.data().likes - 1
      },{merge:true})
    })

  }


  getLikes = (id) => {
    let likes;
    const memeData = this.afs.collection('memes').doc(id).ref;
    memeData.get().then((docSnapshot) => {
         likes = docSnapshot.data().likes
         return likes
    })
  }
}
