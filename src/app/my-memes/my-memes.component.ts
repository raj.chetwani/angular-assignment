import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-my-memes',
  templateUrl: './my-memes.component.html',
  styleUrls: ['./my-memes.component.css']
})
export class MyMemesComponent implements OnInit {

  constructor(public afs: AngularFirestore, private authService: AuthService) { }

  myMemes = []
  bookmarked = this.authService.user.bookmarked
  ngOnInit(): void {

    this.getMyMemes()
    
  }

  async getMyMemes() {
    const snapchot = await this.afs.collection('memes').ref.get();
    return new Promise(resolve => {
        const v = snapchot.docs.map(x => {

            if(x.data().id === this.authService.userID ){
            this.myMemes.push(x.data())
            }       
        });
        resolve(v);
    });
}

}
