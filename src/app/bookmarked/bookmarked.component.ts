import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-bookmarked',
  templateUrl: './bookmarked.component.html',
  styleUrls: ['./bookmarked.component.css']
})
export class BookmarkedComponent implements OnInit {

  memes = []
  bookmarkedMemes = []
  constructor(public authService: AuthService,public afs: AngularFirestore) { }

  ngOnInit(){

   this.getMemes()

  }

  async getMemes() {
    const snapshot = await this.afs.collection('memes').ref.get();
    return new Promise(resolve => {
        const v = snapshot.docs.map(x => {
            if(this.afs.collection('users').doc(this.authService.userID))
            this.memes.push(x.data())
        });
        this.memes.map((meme) => {
            this.authService.user.bookmarked.map((item) => {
                  if(meme.memeID === item){
                    this.bookmarkedMemes.push(meme)
                  }
            })
        })
        resolve(v);
    });
}
  
    removeBookmark = (id) => {
      this.authService.user.bookmarked = this.authService.user.bookmarked.filter((item) => item!== id)

      const dataUser = this.afs.collection('users').doc(this.authService.userID).ref;
    
      dataUser.set({
        bookmarked : this.authService.user.bookmarked
      },{merge:true})

      this.bookmarkedMemes = this.bookmarkedMemes.filter((meme) => meme.memeID !== id)
    }
}
