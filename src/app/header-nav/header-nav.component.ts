import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css']
})

export class HeaderNavComponent implements OnInit {

  user={}
  authenticated = false
  constructor(public authService: AuthService) { 
    
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'))
    
    if(this.user){
      this.authenticated = true
    }else{
      this.authenticated = false
    }

  }
}
